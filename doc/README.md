# App store images

Screenshots have been made with the help of Android's [device art generator](https://developer.android.com/distribute/marketing-tools/device-art-generator).

The app's icon can be found in [../app/src/main/res/mipmap-xxxhdpi/ic_launcher.png](../app/src/main/res/mipmap-xxxhdpi/ic_launcher.png).

For adding images, see [All About Descriptions, Graphics, and Screenshots](https://f-droid.org/en/docs/All_About_Descriptions_Graphics_and_Screenshots/).

Feature graphic background [photograph](https://www.flickr.com/photos/kleuske/8405308133/) is licensed CC-BY-2.0.

Feature graphic has been make with [Inkscape](https://inkscape.org/) by including JPG background and PNG icon via linking. Make sure the paths remain relative path after editing its SVG file.

The resulting feature graphic and screenshots are found in [../fastlane](../fastlane) and will be automatically be picked up by Google Play and F-Droid app stores.
